{{-- sidebar --}}
<div class="app-sidebar colored">
    {{-- sidebar header --}}
    <div class="sidebar-header">
        <a class="header-brand" href="#">
            <div class="logo-img">
                <img src="{{ asset('assets/src/img/brand-white.svg') }}" class="header-brand-img" alt="lavalite">
            </div>
            <span class="text">ThemeKit</span>
        </a>
        <button type="button" class="nav-toggle"><i data-toggle="expanded" class="ik ik-toggle-right toggle-icon"></i></button>
        <button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
    </div>
    {{-- end of sidebar header --}}

    <div class="sidebar-content">
        <div class="nav-container">
            <nav id="main-menu-navigation" class="navigation-main">
                <div class="nav-lavel">Navigation</div>
                <div class="nav-item active">
                    <a href="#">
                        <i class="ik ik-grid"></i><span>Dashboard</span>
                    </a>
                </div>
            </nav>
        </div>
    </div>
</div>
{{-- end of sidebar --}}
{{-- content here --}}
