<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>
            @yield('title')
        </title>
        <meta name="description" content="Aplikasi E-Commerce">
        <meta name="keywords" content="aplikasi e-commerce, aplikasi ecommerce, aplikasi Ecommerce">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {{-- <meta name="CSRF-TOKEN" content="{{ csrf_token }}"> --}}
        @include('layouts.partials.css')
    </head>
    <body>