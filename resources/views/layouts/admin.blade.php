@extends('layouts.partials.main')
@section('title', 'Dashboard')
@section('content')

    {{-- don't use <div class="container"> because is this on container and use class card for display ui--}}
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-md-12">
                            <h3 class="card-title">Visitors By Countries</h3>
                            <div id="visitfromworld" style="width:100%; height:350px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection